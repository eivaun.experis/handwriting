from random import sample
import cv2 as cv
import numpy as np


num_tiles_x = 14
num_tiles_y = 12
tile_size = 64
num_labels = 6
data_file_name = "alpha_64_14.png"

# num_tiles_x = 100
# num_tiles_y = 50
# tile_size = 20
# num_labels = 10
# data_file_name = "digits.png"

bin_n = 16
affine_flags = cv.WARP_INVERSE_MAP | cv.INTER_LINEAR


def deskew(img):
    m = cv.moments(img)
    if abs(m['mu02']) < 1e-2:
        return img.copy()
    skew = m['mu11']/m['mu02']
    M = np.float32([[1, skew, -0.5*tile_size*skew], [0, 1, 0]])
    img = cv.warpAffine(img, M, (tile_size, tile_size), flags=affine_flags)
    return img


def hog(img):
    gx = cv.Sobel(img, cv.CV_32F, 1, 0)
    gy = cv.Sobel(img, cv.CV_32F, 0, 1)
    mag, ang = cv.cartToPolar(gx, gy)
    bins = np.int32(bin_n*ang/(2*np.pi))    # quantizing binvalues in (0...16)
    bin_cells = bins[:10, :10], bins[10:, :10], bins[:10, 10:], bins[10:, 10:]
    mag_cells = mag[:10, :10], mag[10:, :10], mag[:10, 10:], mag[10:, 10:]
    hists = [np.bincount(b.ravel(), m.ravel(), bin_n)
             for b, m in zip(bin_cells, mag_cells)]
    hist = np.hstack(hists)     # hist is a 64 bit vector
    return hist


img = cv.imread(data_file_name, 0)
if img is None:
    raise Exception("we need the digits.png image from samples/data here !")


cells = [np.hsplit(row, num_tiles_x) for row in np.vsplit(img, num_tiles_y)]

half_split = len(cells[0])//2
samples_per_label = len(cells)*half_split // num_labels

# First half is trainData, remaining is testData
train_cells = [i[:half_split] for i in cells]
test_cells = [i[half_split:] for i in cells]


deskewed = [list(map(deskew, row)) for row in train_cells]
hogdata = [list(map(hog, row)) for row in deskewed]
trainData = np.float32(hogdata).reshape(-1, bin_n*4)
responses = np.repeat(np.arange(num_labels), samples_per_label)[:, np.newaxis]

svm = cv.ml.SVM_create()
svm.setKernel(cv.ml.SVM_LINEAR)
svm.setType(cv.ml.SVM_C_SVC)
svm.setC(2.67)
svm.setGamma(5.383)

svm.train(trainData, cv.ml.ROW_SAMPLE, responses)
svm.save('svm_data.dat')


deskewed = [list(map(deskew, row)) for row in test_cells]
hogdata = [list(map(hog, row)) for row in deskewed]
testData = np.float32(hogdata).reshape(-1, bin_n*4)
result = svm.predict(testData)[1]


mask = result == responses
correct = np.count_nonzero(mask)
print(correct*100.0/result.size)
